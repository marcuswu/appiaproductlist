package com.wu.marcus.appiaproductlist.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.wu.marcus.appiaproductlist.R;
import com.wu.marcus.appiaproductlist.adapter.ProductListAdapter;
import com.wu.marcus.appiaproductlist.model.Advertisement;

import java.util.ArrayList;

public class ProductListActivity extends ActionBarActivity {
    private static final String ProductListActivityTag = ProductListActivity.class.getName();
    private static final String productListKey = "PRODUCT_LIST_KEY";

    SwipeRefreshLayout swipeRefreshLayout;
    ProductListAdapter productListAdapter;
    ListView productListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        productListView = (ListView) findViewById(R.id.product_list_view);
        setTitle(R.string.product_list_title);

        if (savedInstanceState != null && savedInstanceState.containsKey(productListKey)) {
            ArrayList<Advertisement> products = savedInstanceState.getParcelableArrayList(productListKey);
            productListAdapter = new ProductListAdapter(this, products);
        } else {
            productListAdapter = new ProductListAdapter(this);
            productListAdapter.refreshProducts(productRefreshCallback);
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }

        productListView.setAdapter(productListAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                productListAdapter.refreshProducts(productRefreshCallback);
            }
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(productListKey, productListAdapter.allProducts());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_list, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (productListAdapter != null) {
            productListAdapter.cancelAllLoaders();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    private ProductListAdapter.OnProductRefresh productRefreshCallback = new ProductListAdapter.OnProductRefresh() {
        @Override
        public void onRefresh(Exception error) {
            swipeRefreshLayout.setRefreshing(false);

            if (error != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(ProductListActivity.this)
                        .create();

                alertDialog.setTitle(R.string.error_message_title);
                alertDialog.setMessage(ProductListActivity.this.getResources()
                        .getString(R.string.network_error_message));

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, ProductListActivity.this
                        .getResources().getString(R.string.alert_ok_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.show();
            }
        }
    };
}
