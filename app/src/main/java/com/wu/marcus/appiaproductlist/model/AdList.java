package com.wu.marcus.appiaproductlist.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * A list of Advertisement for use with simple-xml
 * Created by mwu on 7/20/15.
 */
@Root (name = "ads", strict = false)
public class AdList {
    @ElementList (inline = true)
    ArrayList<Advertisement> adList;

    public ArrayList<Advertisement> getAdList() {
        return adList;
    }

    public void setAdList(ArrayList<Advertisement> adList) {
        this.adList = adList;
    }

}
