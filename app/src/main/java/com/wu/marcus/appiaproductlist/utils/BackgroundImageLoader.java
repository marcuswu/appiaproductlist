package com.wu.marcus.appiaproductlist.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.net.URL;

/**
 * A simple background image loader. This could be extended to handle some caching
 * Created by mwu on 7/20/15.
 */
public class BackgroundImageLoader extends AsyncTask<String, Void, Bitmap> {
    ImageView imageView;
    OnImageLoaded loadedCallback;

    public interface OnImageLoaded {
        void onImageLoaded(ImageView imageView, BackgroundImageLoader loader);
    }

    public BackgroundImageLoader (ImageView imageView, OnImageLoaded loadedCallback) {
        this.imageView = imageView;
        this.loadedCallback = loadedCallback;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        String urlString = urls[0];
        Bitmap image;

        try {
            URL url = new URL(urlString);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            return null;
        }

        return image;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        imageView.setImageBitmap(bitmap);
        loadedCallback.onImageLoaded(imageView, this);
    }
}
