package com.wu.marcus.appiaproductlist.service;

import com.wu.marcus.appiaproductlist.model.AdList;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Interface for a Retrofit REST service for getting a list of ads
 * Created by mwu on 7/20/15.
 */
//http://ads.appia.com/getAds?id=236&password=OVUJ1DJN&siteId=4288&deviceId=4230&sessionId=techtestsession&totalCampaignsRequested=10&lname=wu
public interface ProductListService {
    @GET("/getAds?id=236&password=OVUJ1DJN&siteId=4288&deviceId=4230&sessionId=techtestsession&totalCampaignsRequested=10&lname=wu")
    void getProducts(Callback<AdList> response);
}
