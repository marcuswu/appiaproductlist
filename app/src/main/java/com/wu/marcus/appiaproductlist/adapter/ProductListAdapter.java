package com.wu.marcus.appiaproductlist.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wu.marcus.appiaproductlist.R;
import com.wu.marcus.appiaproductlist.model.AdList;
import com.wu.marcus.appiaproductlist.model.Advertisement;
import com.wu.marcus.appiaproductlist.service.ProductListService;
import com.wu.marcus.appiaproductlist.utils.BackgroundImageLoader;
import com.wu.marcus.appiaproductlist.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.SimpleXMLConverter;

/**
 * ListView adapter for products which can load thumbnails
 * Created by mwu on 7/20/15.
 */
public class ProductListAdapter extends BaseAdapter {
    private static final String ProductListAdapterTag = ProductListAdapter.class.getName();
    private final Context context;
    ProductListService productService;
    ArrayList<Advertisement> productList = new ArrayList<>();
    HashMap<ImageView, BackgroundImageLoader> backgroundImageLoaders = new HashMap<>();
    boolean loading = false;

    public ProductListAdapter(Context context) {
        this.context = context;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://ads.appia.com")
                .setConverter(new SimpleXMLConverter())
                .build();
        productService = restAdapter.create(ProductListService.class);
    }

    public ProductListAdapter(Context context, ArrayList<Advertisement> ads) {
        this.context = context;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://ads.appia.com")
                .setConverter(new SimpleXMLConverter())
                .build();
        productService = restAdapter.create(ProductListService.class);

        productList = ads;
    }

    public ArrayList<Advertisement> allProducts() {
        return productList;
    }

    public interface OnProductRefresh {
        void onRefresh(Exception error);
    }

    public void refreshProducts(final OnProductRefresh refreshCallback) {
        if (NetworkUtils.haveConnection(context) && !loading) {
            loading = true;
            Log.d(ProductListAdapterTag, "Have connection. Requesting products");
            productService.getProducts(new Callback<AdList>() {
                @Override
                public void success(AdList adList, Response response) {
                    Log.d(ProductListAdapterTag, "Succeeded to load product list");
                    productList = adList.getAdList();
                    ProductListAdapter.this.notifyDataSetChanged();
                    loading = false;
                    refreshCallback.onRefresh(null);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(ProductListAdapterTag, "Failed to load product list: "+error);
                    productList = new ArrayList<>();
                    loading = false;
                    refreshCallback.onRefresh(error);
                }
            });
        } else {
            // It would be nice to actually display different messages per error
            refreshCallback.onRefresh(new Exception("No network connection"));
            Log.d(ProductListAdapterTag, "No connection. Cannot request products.");
        }
    }

    @SuppressWarnings("unused")
    public boolean isLoading() {
        return loading;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    public void cancelAllLoaders() {
        ArrayList<ImageView> viewsWithLoaders = new ArrayList<>(backgroundImageLoaders.keySet());
        for (ImageView imageView:viewsWithLoaders) {
            backgroundImageLoaders.get(imageView).cancel(true);
            backgroundImageLoaders.remove(imageView);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View resultView = convertView;

        if (resultView == null) {
            // inflate a new view
            resultView = inflater.inflate(R.layout.product_list_element, parent, false);
        } else {
            // There may be an image loader to cancel
            ImageView thumbnailView = (ImageView) resultView.findViewById(R.id.product_thumbnail);
            thumbnailView.setImageBitmap(null);
            if (backgroundImageLoaders.containsKey(thumbnailView)) {
                BackgroundImageLoader imageLoader = backgroundImageLoaders.get(thumbnailView);
                if (imageLoader != null) {
                    imageLoader.cancel(true);
                }
                backgroundImageLoaders.remove(thumbnailView);
            }
        }

        if (resultView != null) {
            ImageView thumbnailView = (ImageView) resultView.findViewById(R.id.product_thumbnail);
            TextView productNameView = (TextView) resultView.findViewById(R.id.product_name);
            TextView minOsVersionView = (TextView) resultView.findViewById(R.id.min_os_version);

            Advertisement ad = productList.get(position);
            BackgroundImageLoader imageLoader = new BackgroundImageLoader(thumbnailView, loaderOnComplete);
            backgroundImageLoaders.put(thumbnailView, imageLoader);
            imageLoader.execute(ad.getThumbnailUrl());

            productNameView.setText(ad.getProductName());
            minOsVersionView.setText(ad.getMinOsVersion());
        }

        return resultView;
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return productList.get(position).getId();
    }

    private final BackgroundImageLoader.OnImageLoaded loaderOnComplete = new BackgroundImageLoader.OnImageLoaded() {
        @Override
        public void onImageLoaded(ImageView imageView, BackgroundImageLoader loader) {
            if (backgroundImageLoaders.containsKey(imageView)) {
                backgroundImageLoaders.remove(imageView);
            }
        }
    };
}
