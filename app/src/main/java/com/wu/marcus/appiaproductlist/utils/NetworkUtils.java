package com.wu.marcus.appiaproductlist.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Small utility to check network connectivity
 * Created by mwu on 7/20/15.
 */
public class NetworkUtils {
    /**
     * Check to see if we have a connection
     * Ideally in a real application this would be handled in a less involved manner so that we
     * don't have to write code to check for connectivity for each network call
     * @param context An Android context
     * @return whether we have a connection or not
     */
    public static boolean haveConnection(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
}
