package com.wu.marcus.appiaproductlist.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Represent an advertisement returned from the REST service
 * Created by mwu on 7/20/15.
 */
@Root (name = "ad", strict = false)
public class Advertisement implements Parcelable {
    @Element (name = "campaignId")
    long id;

    @Element (name = "minOSVersion", required = false)
    String minOsVersion;

    @Element (name = "productName")
    String productName;

    @Element (name = "productThumbnail")
    String thumbnailUrl;

    public Advertisement() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMinOsVersion() {
        return minOsVersion;
    }

    public void setMinOsVersion(String minOsVersion) {
        this.minOsVersion = minOsVersion;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Advertisement> CREATOR = new Parcelable.Creator<Advertisement>() {
        @Override
        public Advertisement createFromParcel(Parcel source) {
            return new Advertisement(source);
        }

        @Override
        public Advertisement[] newArray(int size) {
            return new Advertisement[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(productName);
        dest.writeString(thumbnailUrl);
        dest.writeString(minOsVersion);
    }

    private Advertisement(Parcel in) {
        id = in.readLong();
        productName = in.readString();
        thumbnailUrl = in.readString();
        minOsVersion = in.readString();
    }
}
